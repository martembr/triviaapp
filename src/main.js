import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Welcome from './views/Welcome'
import ResultInfo from './views/ResultInfo'
import Question from './views/Question'


Vue.use(VueRouter);

const routes = [
  { path: '/', component: Welcome, name: "Welcome" },
  { path: '/result', component: ResultInfo, name: "ResultInfo" },
  { path: '/question', component: Question, name: "Question" }
]

const router = new VueRouter( {
  routes
});

Vue.config.productionTip = false


new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
