export const getAllQuestions = () => {
    return fetch("https://opentdb.com/api.php?amount=10&difficulty=medium")
    .then(resp => resp.json())
    .then( resp => {
        if (resp.success == false) {
            throw Error(resp.error);
        }
        return resp;})
    .then(resp => resp.results);
}
